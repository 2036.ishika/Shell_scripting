#!/bin/bash

add(){
    echo "addition: $(( $num1 + $num2))"
}
sub(){
    echo "subtraction: $(( $num1 - $num2))"
}
mul(){
    echo "multiplication: $(( $num1 * $num2))"
}
div(){
    echo "division: $(( $num1 / $num2))"
}




operand=$1
num1=$2
num2=$3

case $operand in
    add)
        add $num1 $num2
        ;;
    subtract)
        sub $num1 $num2
        ;;
    multiply)
        mul $num1 $num2
        ;;
    div)
        div $num1 $num2
        ;;
    *)
        echo "Invalid Operation"
        ;;
esac