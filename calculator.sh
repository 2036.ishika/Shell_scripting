#!/bin/bash

operation= $1
num1= $2
num2= $3

if [ "$operation" = "add" ]; then
    result= $(($num1 +$num2 ))
    echo "sum: $result"
elif [ "$operation" = "sub" ]; then
    result= $(($num1 - $num2 ))
    echo "subtract: $result"
elif [ "$operation" = "multiply" ]; then
    result= $(($num1 * $num2 ))
    echo "multiplication: $result"
elif [ "$operation" = "div" ]; then
    result= $(($num1 / $num2 ))
    echo "division: $result"
else
echo "Invalid Operation"

fi

